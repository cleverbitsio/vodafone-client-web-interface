<!DOCTYPE html>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html lang="en">
<head>
    <script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
	<!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</head>

<body onload="UserAction(1)">
	<div class="container">
		<div class="starter-template">
			<h2>Vodafone Load Generator</h2>
		</div>
    </div>
    <div class="container">
        <form class="form-inline" action="#" method="post">
            <div class="form-group">
		        <label for="textInput">Writes Per Second</label>
		        <input type="text" id="textInput" value=1 onchange="updateRangeInput(this.value);">
                <input type="range" id="rangeInput" name="rangeInput" min="0" max="10000" onchange="updateTextInput(this.value);" value=1>
            </div>
        </form>
	</div>

    <script type="text/javascript">
        function updateTextInput(val) {
          document.getElementById('textInput').value=val;
          UserAction(val);
        }

        function updateRangeInput(val) {
          document.getElementById("rangeInput").value=val;
         if(val >10000) val = 10000
         else if (val < 1) val = 1;
         document.getElementById("textInput").value=val;
         UserAction(val);
        }

        function UserAction(val) {
            var xhttp = new XMLHttpRequest();
            xhttp.open("GET", "https://localhost:8443/runBashFile?writespersecond=" + val, true);
            xhttp.setRequestHeader("Content-type", "application/json");
            xhttp.withCredentials = true;
            xhttp.onreadystatechange = function() {
                if (this.readyState == 4 && this.status == 200) {
                    var myObj = JSON.parse(this.responseText);
                    console.log(myObj.writespersecond);
                }
            };
            xhttp.send();
        }
    </script>
</body>
</html>
